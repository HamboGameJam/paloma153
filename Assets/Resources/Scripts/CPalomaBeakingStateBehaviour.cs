﻿using UnityEngine;
using System.Collections;

public class CPalomaBeakingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;

	private bool           m_bOnEnabledIsOnAir;
	private bool           m_bOnEnabledIsRunning;
	private bool           m_bOnEnabledIsWalking;

	private bool           m_bStartedBeaking;

	override public void OnEnabled()
	{
		this.enabled = true;

		m_playablePaloma.m_paloma.bIsBeaking = true;
		m_bStartedBeaking = false;
		m_bOnEnabledIsOnAir   = m_playablePaloma.m_paloma.bIsOnAir;
		m_bOnEnabledIsRunning = m_playablePaloma.m_paloma.bIsRunning;
		m_bOnEnabledIsWalking = m_playablePaloma.m_paloma.bIsWalking;

	}
	
	override public void OnDisabled()
	{
		m_playablePaloma.m_paloma.bIsBeaking = false;
		this.enabled = false;

	}

	void Start ()
	{
	
	}
	
	void FixedUpdate()
	{	
		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );

		if(  ( animatorStateInfoCurrent.nameHash !=
		       Animator.StringToHash("Base Layer.stateBeaking") ) &&
		     ( m_bStartedBeaking                                )     )
		{
			if( m_bOnEnabledIsOnAir )
			{
				m_statemachineParentReference.stateActive = "Flapping";
			}
			if( m_bOnEnabledIsRunning )
			{
				m_statemachineParentReference.stateActive = "Running";
			}
			else if( m_bOnEnabledIsWalking )
			{
				m_statemachineParentReference.stateActive = "Walking";
			}
			else
			{
				m_statemachineParentReference.stateActive = "Idle";
			}			
		}
		else
		{
			m_bStartedBeaking = true;
		}

	}


}