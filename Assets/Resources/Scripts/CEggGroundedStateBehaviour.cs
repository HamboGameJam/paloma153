using UnityEngine;
using System.Collections;

public class CEggGroundedStateBehaviour : StateBehaviour
{
	public CPlayableEgg m_playableEgg;

	override public void OnEnabled()
	{
		this.enabled = true;
		
		this.m_playableEgg.m_egg.bIsBreaking  = false;
		this.m_playableEgg.m_egg.bIsSpawning  = false;

		this.m_playableEgg.m_eggController.isMovingInGround = false;
		
		this.m_playableEgg.m_eggController.m_fSpeedCurrent = 0.00f;

	}


	override public void OnDisabled()
	{
		this.enabled = false;
	}


	void Start()
	{
	
	}


	void FixedUpdate()
	{
		
		if(  ( this.m_playableEgg.m_eggController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                             ) == 0  )
		{
			m_statemachineParentReference.stateActive = "Falling";
		}
		else
		{
			this.m_playableEgg.m_eggController.m_fSpeedVertical = 0.00f;
			
		}
		
	}


}