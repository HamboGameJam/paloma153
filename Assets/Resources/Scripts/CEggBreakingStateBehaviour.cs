﻿using UnityEngine;
using System.Collections;

public class CEggBreakingStateBehaviour : StateBehaviour
{
	public CPlayableEgg m_playableEgg;

	override public void OnEnabled()
	{
		this.enabled = true;

		this.m_playableEgg.m_egg.bIsOnAir    = false;
		this.m_playableEgg.m_egg.bIsBreaking = true;
		this.m_playableEgg.m_egg.bIsSpawning = false;
		
		Debug.Log("CEggBreakingStateBehaviour:OnEnabled");
		
	}
	
	
	override public void OnDisabled()
	{
		this.enabled = false;
	}


	void Start()
	{
	
	}

	void Update()
	{
		AnimatorStateInfo animatorStateInfoCurrent =
			m_playableEgg.m_egg.animator.GetCurrentAnimatorStateInfo( 0 );
		
		if(  animatorStateInfoCurrent.nameHash !=
			 Animator.StringToHash("Base Layer.animationEggBreak")  )
		{
			GameObject.Destroy( gameObject, 1.0f );
		}	
	
	}


}