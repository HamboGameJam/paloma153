﻿using UnityEngine;
using System.Collections;

public class CPalomaJumpingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		m_playablePaloma.m_paloma.bIsOnAir = true;
		m_playablePaloma.m_paloma.bIsJumping = true;
	}
	
	override public void OnDisabled()
	{	
		m_playablePaloma.m_paloma.bIsJumping = false;
		this.enabled = false;		
	}
	
	void Start()
	{
		
	}
	
	void Update()
	{
		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );
		
		if(  animatorStateInfoCurrent.nameHash !=
		     Animator.StringToHash("Base Layer.stateJumping")  )
		{
			m_statemachineParentReference.stateActive = "Flapping";
		}	

	}

	void FixedUpdate()
	{
		m_playablePaloma.m_paloma.fVerticalSpeed =
			m_playablePaloma.m_extendedController.verticalSpeed +
			m_playablePaloma.m_extendedController.currentSpeed;
	}
	
	
}