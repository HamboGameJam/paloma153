﻿using UnityEngine;
using System.Collections;

public class CPlayableEgg : MonoBehaviour
{
	public string id;
	public EggController m_eggController;
	public CEgg          m_egg;
	public StateMachine  m_stateMachine;

	void Awake()
	{
		m_eggController =
			gameObject.GetComponent<EggController>();

		m_eggController.gravity       =  9.80f;
		m_eggController.verticalSpeed = 00.00f;

		m_egg = 
			gameObject.GetComponent<CEgg>();

		///////////////////////////////////////
		// Create State Machine 
		m_stateMachine =
			gameObject.AddComponent<StateMachine>();

		
		///////////////////////////////////////
		// Create the States
		// - A state is created by simpply Naming it... 
		State stateSpawning   = m_stateMachine.StateInsert( "Spawning"   );
		State stateGrounded   = m_stateMachine.StateInsert( "Grounded"   );
		State stateFalling    = m_stateMachine.StateInsert( "Falling"    );
		State stateBreaking   = m_stateMachine.StateInsert( "Breaking"   );
		//State stateDestroying = m_stateMachine.StateInsert( "Destroying" );
		///////////////////////////////////////
		// Load the Scripts
		// - Add Component to create an instantiation of the script
		// - Assign the reference of the instantiated StateMachine to the StateMachineParent
		//   In order to indicate to the state machine to change the state within another state script

		CEggSpawningStateBehaviour eggSpawningStateBehaviour =
			gameObject.AddComponent("CEggSpawningStateBehaviour") as CEggSpawningStateBehaviour;
		( (StateBehaviour)eggSpawningStateBehaviour ).stateMachineParent = m_stateMachine;
		eggSpawningStateBehaviour.m_playableEgg = this;

		CEggGroundedStateBehaviour eggGroundedStateBehaviour =
			gameObject.AddComponent("CEggGroundedStateBehaviour") as CEggGroundedStateBehaviour;
		( (StateBehaviour)eggGroundedStateBehaviour ).stateMachineParent = m_stateMachine;
		eggGroundedStateBehaviour.m_playableEgg = this;

		CEggFallingStateBehaviour eggFallingStateBehaviour =
			gameObject.AddComponent("CEggFallingStateBehaviour") as CEggFallingStateBehaviour;
		( (StateBehaviour)eggFallingStateBehaviour ).stateMachineParent = m_stateMachine;
		eggFallingStateBehaviour.m_playableEgg = this;

		CEggBreakingStateBehaviour eggBreakingStateBehaviour =
			gameObject.AddComponent("CEggBreakingStateBehaviour") as CEggBreakingStateBehaviour;
		( (StateBehaviour)eggBreakingStateBehaviour ).stateMachineParent = m_stateMachine;
		eggBreakingStateBehaviour.m_playableEgg = this;

		///////////////////////////////////////
		// Assignt the scripts to the states
		// - There can be only one state active at a time within a particular state machine
		// - A single instantiation of a script can be assigned to multiple states
		stateSpawning.ScriptInsert( eggSpawningStateBehaviour );
		stateGrounded.ScriptInsert( eggGroundedStateBehaviour );
		stateFalling.ScriptInsert( eggFallingStateBehaviour );
		stateBreaking.ScriptInsert( eggBreakingStateBehaviour );


		//////////////////////////////////////
		// set the previous state and the current one
		m_stateMachine.stateActive = "Spawning";
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
