﻿using UnityEngine;
using System.Collections;

public class CPlayablePalomaFlappingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;

	private bool m_bIsButtonBeakingPushed;
	private bool m_bIsButtonFlappingPushed;
	private bool m_bIsButtonRunningPushed; 

	override public void OnEnabled()
	{
		this.enabled = true;

		Debug.Log("CPlayablePalomaFlappingStateBehaviour:OnEnabled");
	}


	override public void OnDisabled()
	{
		this.enabled = false;

		Debug.Log("CPlayablePalomaFlappingStateBehaviour:OnDisabled");
	}


	void Start()
	{
	
	}
	

	private void Update_ProcessInput_AxisHorizontal()
	{
		float fInputAxisHorizontal = Input.GetAxis( ""+m_playablePaloma.id+"_Horizontal" );
		
		if( fInputAxisHorizontal != 0.00f )
		{
			if( fInputAxisHorizontal < 0.00f )
			{
				if( m_playablePaloma.m_paloma.bIsFacingLeft != true )
				{
					m_playablePaloma.m_extendedController.currentSpeed *= 0.550f;
				}

				m_playablePaloma.m_paloma.bIsFacingLeft = true;

			}
			else if( fInputAxisHorizontal > 0.00 )
			{

				if( m_playablePaloma.m_paloma.bIsFacingLeft != false )
				{
					m_playablePaloma.m_extendedController.currentSpeed *= 0.55f;
				}

				m_playablePaloma.m_paloma.bIsFacingLeft = false;

			} 
		}

	}

	private void Update_ProcessInput_ButtonBeak()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Beak" ) )
		{
			m_bIsButtonBeakingPushed = true;
		}
		
	}

	private void Update_ProcessInput_ButtonFlap()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Flap" ) )
		{
			m_bIsButtonFlappingPushed = true;
		}
		
	}

	private void Update_ProcessInput_ButtonRun()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Run" ) )
		{
			m_bIsButtonRunningPushed = true;			
		}

	}

	private void Update_ProcessInput()
	{
		if( this.m_bIsButtonBeakingPushed )
		{
			m_statemachineParentReference.stateActive = "Beaking";
		}

		if( this.m_bIsButtonFlappingPushed )
		{
			if( this.m_bIsButtonRunningPushed )
			{
				m_playablePaloma.m_extendedController.currentSpeed = 
					m_playablePaloma.m_extendedController.runSpeed;
			}
			else
			{
				m_playablePaloma.m_extendedController.currentSpeed =
					m_playablePaloma.m_extendedController.walkSpeed;
			}

			m_statemachineParentReference.stateActive = "Jumping";
		}




	}


	void Update()
	{
		this.m_bIsButtonFlappingPushed = false;
		this.m_bIsButtonRunningPushed  = false;
		this.m_bIsButtonBeakingPushed  = false;


		Update_ProcessInput_ButtonBeak();
		Update_ProcessInput_ButtonFlap();
		Update_ProcessInput_ButtonRun();
		Update_ProcessInput();
		Update_ProcessInput_AxisHorizontal();

	}
	

}