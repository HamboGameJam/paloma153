﻿using UnityEngine;
using System.Collections;

public class CPlayerSpawnPoint : MonoBehaviour
{
	public string id;

	void Awake()
	{
		GameObject gameObjectPaloma = 
			GameObject.Instantiate(  Resources.Load( "Prefabs/prefabPaloma" )  ) as GameObject;

		gameObjectPaloma.GetComponent<CPlayablePaloma>().id = 
			this.id;

		gameObjectPaloma.name = "paloma_"+this.id;

		Vector3 v3LocalPosition = new Vector3( this.transform.localPosition.x,
		                                       this.transform.localPosition.y,
		                                       this.transform.localPosition.z  );

		gameObjectPaloma.transform.localPosition = v3LocalPosition;

	}

	void Start()
	{
	
	}
	
	void Update()
	{
	
	}

}