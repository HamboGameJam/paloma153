﻿using UnityEngine;
using System.Collections;

public class CPlayablePalomaIdleStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	private bool bIsRunButtonPressed;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CPlayablePalomaIdleStateBehaviour:OnEnabled");
	}


	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CPlayablePalomaIdleStateBehaviour:OnDisabled");
	}


	void Start()
	{
		
	}
	
	
	private void Update_ProcessInput_AxisHorizontal()
	{
		float fInputAxisHorizontal = Input.GetAxis( ""+m_playablePaloma.id+"_Horizontal" );

		if( fInputAxisHorizontal != 0.00f )
		{
			if( fInputAxisHorizontal < 0.00f )
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = true;				
			}
			else if( fInputAxisHorizontal > 0.00 )
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = false;				
			}

			if( bIsRunButtonPressed )
			{
				m_statemachineParentReference.stateActive = "Running";
			}
			else
			{
				m_statemachineParentReference.stateActive = "Walking";
			}
		}
		
	}


	private void Update_ProcessInput_ButtonBeak()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Beak" ) )
		{
			m_statemachineParentReference.stateActive = "Beaking";
		}

	}


	private void Update_ProcessInput_ButtonFlap()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Flap" ) )
		{
			m_statemachineParentReference.stateActive = "Jumping";
		}
		
	}


	private void Update_ProcessInput_ButtonRun()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Run" ) )
		{
			bIsRunButtonPressed = true;
		}
		
	}


	void Update()
	{
		bIsRunButtonPressed = false;

		Update_ProcessInput_ButtonBeak();
		Update_ProcessInput_ButtonRun();
		Update_ProcessInput_AxisHorizontal();
		Update_ProcessInput_ButtonFlap();
				
	}

	
}