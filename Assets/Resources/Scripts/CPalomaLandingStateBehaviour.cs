﻿using UnityEngine;
using System.Collections;

public class CPalomaLandingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		m_playablePaloma.m_paloma.bIsOnAir   = false;
		m_playablePaloma.m_extendedController.isMovingInGround  = false;
		m_playablePaloma.m_paloma.bIsFalling = false;
		m_playablePaloma.m_paloma.bIsLanding = true;

		Debug.Log("CPalomaLandingStateBehaviour:OnEnabled");
	}
	
	override public void OnDisabled()
	{
		//m_playablePaloma.m_paloma.bIsLanding = false;
		this.enabled = false;
		Debug.Log("CPalomaLandingStateBehaviour:OnDisabled");
	}

	void Start ()
	{
	
	}	

	void Update ()
	{
		/*
		m_playablePaloma.m_extendedController.m_fSpeedVertical = 0.00f;	

		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );

		if(  animatorStateInfoCurrent.nameHash !=
			 Animator.StringToHash("Base Layer.stateLanding")  )
		{
			m_statemachineParentReference.stateActive = "Idle";
		}
		*/

		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0 )
		{
			m_statemachineParentReference.stateActive = "Falling";
		}

		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );
		if(  animatorStateInfoCurrent.nameHash !=
		     Animator.StringToHash("Base Layer.stateLanding")  )
		{
			m_statemachineParentReference.stateActive = "Idle";
		}

	}


}