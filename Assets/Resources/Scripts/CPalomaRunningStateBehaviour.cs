﻿using UnityEngine;
using System.Collections;

public class CPalomaRunningStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		m_playablePaloma.m_extendedController.currentSpeed =
			m_playablePaloma.m_extendedController.runSpeed;
		
		m_playablePaloma.m_extendedController.verticalSpeed = 
			0.00f;
		
		m_playablePaloma.m_paloma.bIsRunning =
			true;

		Debug.Log("CPalomaRunningStateBehaviour:OnEnabled");

	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CPalomaRunningStateBehaviour:OnDisabled");

	}
	
	void Start()
	{
		
	}

	void FixedUpdate()
	{		
		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0  )
		{
			m_statemachineParentReference.stateActive = "Falling";
		}
		else
		{
			m_playablePaloma.m_extendedController.m_fSpeedVertical = 0.00f;
		}
		
	}

}
