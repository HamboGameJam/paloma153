using UnityEngine;

public class CPlayablePalomaWalkingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	private bool m_bIsButtonBeakingPushed;

	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CPlayablePalomaWalkingStateBehaviour:OnEnabled");
	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CPlayablePalomaWalkingStateBehaviour:OnDisabled");
	}
	
	void Start()
	{
		
	}
	
	private void Update_ProcessInput_AxisHorizontal()
	{
		float fInputAxisHorizontal = Input.GetAxis( ""+m_playablePaloma.id+"_Horizontal" );
		
		if( fInputAxisHorizontal < 0.00f )
		{
			m_playablePaloma.m_paloma.bIsFacingLeft = true;
		}
		else if( fInputAxisHorizontal > 0.00 )
		{
			m_playablePaloma.m_paloma.bIsFacingLeft = false;
		}
		else
		{
			m_statemachineParentReference.stateActive = "Idle";
		}
	}
	
	private void Update_ProcessInput_ButtonBeak()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Beak" ) )
		{
			m_statemachineParentReference.stateActive = "Beaking";
		}
	}

	private void Update_ProcessInput_ButtonFlap()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Flap" ) )
		{
			m_statemachineParentReference.stateActive = "Jumping";
		}
		
	}
	
	private void Update_ProcessInput_ButtonRun()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Run" ) )
		{
			m_statemachineParentReference.stateActive = "Running";
		}
		
	}


	void Update()
	{
		Update_ProcessInput_AxisHorizontal();
		Update_ProcessInput_ButtonBeak();
		Update_ProcessInput_ButtonFlap();
		Update_ProcessInput_ButtonRun();

	}
	
}