﻿using UnityEngine;
using System.Collections;

public class CPlayablePalomaRunningStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	//private bool m_bIsButtonRunningPressed;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		//m_bIsButtonRunningPressed = false;
		Debug.Log("CPlayablePalomaRunningStateBehaviour:OnEnabled");
	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CPlayablePalomaRunningStateBehaviour:OnDisabled");
	}
	
	void Start()
	{
		
	}
	
	private void Update_ProcessInput_AxisHorizontal()
	{
		float fInputAxisHorizontal = Input.GetAxis( ""+m_playablePaloma.id+"_Horizontal" );
		
		if( fInputAxisHorizontal < 0.00f )
		{
			if( !m_playablePaloma.m_paloma.bIsFacingLeft  )
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = true;
				m_statemachineParentReference.stateActive = "Breaking";
			}
			else
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = true;
			}

		}
		else if( fInputAxisHorizontal > 0.00 )
		{
			if(  m_playablePaloma.m_paloma.bIsFacingLeft  )
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = false;
				m_statemachineParentReference.stateActive = "Breaking";
			}
			else
			{
				m_playablePaloma.m_paloma.bIsFacingLeft = false;
			}

		}
		else
		{
			m_statemachineParentReference.stateActive = "Breaking";
		}

	}

	private void Update_ProcessInput_ButtonBeak()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Beak" ) )
		{
			m_statemachineParentReference.stateActive = "Beaking";
		}
		
	}

	private void Update_ProcessInput_ButtonFlap()
	{
		if( Input.GetButton( ""+m_playablePaloma.id+"_Flap" ) )
		{
			m_statemachineParentReference.stateActive = "Jumping";
		}
		
	}
	
	private void Update_ProcessInput_ButtonRun()
	{
		if( !Input.GetButton( ""+m_playablePaloma.id+"_Run" ) )
		{
			//m_bIsButtonRunningPressed = true;
			m_statemachineParentReference.stateActive = "Walking";
		}
		
	}

	void Update()
	{
		//m_bIsButtonRunningPressed = false;
		Update_ProcessInput_ButtonBeak();
		Update_ProcessInput_ButtonFlap();
		Update_ProcessInput_ButtonRun();

		Update_ProcessInput_AxisHorizontal();
		
	}


}