﻿using UnityEngine;
using System.Collections;

public class CRotate : MonoBehaviour
{
	public float   RotationSpeed;
	public bool    IsRotating;
	public Vector3 RotationAxis;
	private Quaternion quaternionRotation;

	void Awake()
	{
		//RotationAxis = new Vector3 ();
	}

	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}

	void FixedUpdate()
	{
		if( IsRotating )
		{
			quaternionRotation = Quaternion.AngleAxis( (Time.deltaTime * RotationSpeed), RotationAxis );

			gameObject.transform.rotation =
				gameObject.transform.rotation * quaternionRotation;
			//Debug.Log("IsRotating"+IsRotating);
		}
	}
}
