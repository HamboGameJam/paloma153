﻿using UnityEngine;
using System.Collections;

public class CPalomaIdleStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;

		this.m_playablePaloma.m_paloma.bIsRunning  = false;
		this.m_playablePaloma.m_paloma.bIsFalling  = false;
		this.m_playablePaloma.m_paloma.bIsBreaking = false;
		this.m_playablePaloma.m_paloma.bIsOnAir    = false;
		this.m_playablePaloma.m_paloma.bIsLanding  = false;
		this.m_playablePaloma.m_extendedController.isMovingInGround = false;

		m_playablePaloma.m_extendedController.m_fSpeedCurrent = 0.00f;
	}

	override public void OnDisabled()
	{
		this.enabled = false;
	}

	void Start()
	{
	
	}

	void FixedUpdate()
	{

		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0  )
		{
			this.m_playablePaloma.m_paloma.bIsOnAir = true;
			m_statemachineParentReference.stateActive = "Falling";
		}
		else
		{
			m_playablePaloma.m_extendedController.m_fSpeedVertical = 0.00f;

		}

	}


}