﻿using UnityEngine;
using System.Collections;

public class CTestLevelInitStateBehaviour : StateBehaviour
{
	public CSharedData  m_sharedData;

	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CTestLevelInitStateBehaviour:OnEnabled");

		Initialize();

		m_statemachineParentReference.stateActive = "Update";
	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CTestLevelInitStateBehaviour:OnDisabled");
	}

	private void Initialize()
	{
		InitializeSpawnPointsGet();
		InitializeSpawnPointsShuffle(); 
		InitializeSpawnPointsSetPlayers();
		InitializeAddFollowCamerasToPlayers();
	  
	}

	private void InitializeSpawnPointsGet()
	{
		this.m_sharedData.arrGameObjectSpawnPoints =
			GameObject.FindGameObjectsWithTag( "Respawn" );
	}

	private void InitializeSpawnPointsShuffle()
	{
		for( int iSpawnIndex = this.m_sharedData.arrGameObjectSpawnPoints.Length - 1;
				 iSpawnIndex > 0;
		    	 iSpawnIndex --                                                       )
		{
			int iNewRandomSpawnIndex =
				Random.Range( 0, iSpawnIndex );

			GameObject gameObjectSpawnRandom =
				this.m_sharedData.arrGameObjectSpawnPoints[ iNewRandomSpawnIndex ];

			this.m_sharedData.arrGameObjectSpawnPoints[iNewRandomSpawnIndex] =
				this.m_sharedData.arrGameObjectSpawnPoints[iSpawnIndex];

			this.m_sharedData.arrGameObjectSpawnPoints[iSpawnIndex] =
				gameObjectSpawnRandom;
		}
	}

	private GameObject InitializeSpawnPointsSetPlayer( GameObject gameObjectSpawnPoint,
	                                             	   string strPlayerId               )
	{
		GameObject gameObjectPaloma = 
			GameObject.Instantiate(  Resources.Load( "Prefabs/prefabPaloma" )  ) as GameObject;
		
		gameObjectPaloma.GetComponent<CPlayablePaloma>().id = 
			strPlayerId;
		
		gameObjectPaloma.name = "paloma_"+strPlayerId;
		
		Vector3 v3LocalPosition = new Vector3( gameObjectSpawnPoint.transform.localPosition.x,
		                                       gameObjectSpawnPoint.transform.localPosition.y,
		                                       gameObjectSpawnPoint.transform.localPosition.z  );
		
		gameObjectPaloma.transform.localPosition = v3LocalPosition;


		CSpawnPoint spawnPointPlayerTaken =
			gameObjectSpawnPoint.GetComponent<CSpawnPoint>();

		spawnPointPlayerTaken.bIsUsed = true;

		return gameObjectPaloma;
	}


	private void InitializeSpawnPointsSetPlayers()
	{	
		this.m_sharedData.arrGameObjectPalomaPlayers =
			new GameObject[2];

		this.m_sharedData.arrGameObjectPalomaPlayers[0] = 
			InitializeSpawnPointsSetPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 0 ], "P0" );

		this.m_sharedData.arrGameObjectPalomaPlayers[1] = 
			InitializeSpawnPointsSetPlayer( this.m_sharedData.arrGameObjectSpawnPoints[ 1 ], "P1" );
	}

	private GameObject InitializeAddFollowCameraToPlayer( ref GameObject gameObjectPlayer,
	                                                      float     fViewPortRectX,
	                                                      float     fViewPortRectY,
	                                                      float     fViewPortRectWidth,
	                                                      float     fViewPortRectHeight )
	{
		GameObject gameObjectCamera = 
			GameObject.Instantiate(  Resources.Load( "Prefabs/prefabCamera" )  ) as GameObject;

		CCameraFollow cameraFollow =
			gameObjectCamera.GetComponent<CCameraFollow>();

		Vector3 v3CameraPosition = 
			new Vector3( gameObjectPlayer.transform.position.x,
			             gameObjectPlayer.transform.position.y,
			             cameraFollow.m_v3PositionDestination.z );

		gameObjectCamera.transform.position = v3CameraPosition;

		cameraFollow.m_gameObjectTarget  = gameObjectPlayer;
		cameraFollow.fViewPortRectX      = fViewPortRectX;
		cameraFollow.fViewPortRectY      = fViewPortRectY;
		cameraFollow.fViewPortRectWidth  = fViewPortRectWidth;
		cameraFollow.fViewPortRectHeight = fViewPortRectHeight;

		return gameObjectCamera;
	}

	private void InitializeAddFollowCamerasToPlayers( )
	{
		this.m_sharedData.arrGameObjectFollowCameras =
			new GameObject[2];

		this.m_sharedData.arrGameObjectFollowCameras[0] =
			InitializeAddFollowCameraToPlayer( ref this.m_sharedData.arrGameObjectPalomaPlayers[0],
			                                   0.00f /*fViewPortRectX*/,
			                                   0.00f /*fViewPortRectY*/,
			                                   0.50f /*fViewPortRectWidth*/,
			                                   1.00f /*fViewPortRectHeight*/                              );

		this.m_sharedData.arrGameObjectFollowCameras[1] =
			InitializeAddFollowCameraToPlayer( ref this.m_sharedData.arrGameObjectPalomaPlayers[1],
			                                   0.50f /*fViewPortRectX*/,
			                                   0.00f /*fViewPortRectY*/,
			                                   0.50f /*fViewPortRectWidth*/,
			                                   1.00f /*fViewPortRectHeight*/                              );
	}
	
}
