﻿using UnityEngine;
using System.Collections;

public class CEggSpawningStateBehaviour : StateBehaviour
{
	public CPlayableEgg m_playableEgg;

	override public void OnEnabled()
	{
		this.enabled = true;
		m_playableEgg.m_egg.bIsSpawning = true;
		//m_playableEgg.m_eggController.m_fHealth = 1.00f;

	}
	
	
	override public void OnDisabled()
	{
		m_playableEgg.m_egg.bIsSpawning = false;
		this.enabled = false;
	}


	void Start() 
	{
	
	}
	

	void FixedUpdate()
	{
		AnimatorStateInfo animatorStateInfoCurrent =
			m_playableEgg.m_egg.animator.GetCurrentAnimatorStateInfo( 0 );
		
		if(  animatorStateInfoCurrent.nameHash !=
		   	 Animator.StringToHash("Base Layer.animationEggSpawning")  )
		{
			m_statemachineParentReference.stateActive = "Falling";
		}

	}


}
