﻿using UnityEngine;
using System.Collections;

public class CTestLevelUpdateStateBehaviour : StateBehaviour
{
	public CSharedData  m_sharedData;

	override public void OnEnabled()
	{
		this.enabled = true;
		Debug.Log("CTestLevelUpdateStateBehaviour:OnEnabled");

	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CTestLevelUpdateStateBehaviour:OnDisabled");

	}


}