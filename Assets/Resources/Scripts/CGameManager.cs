using UnityEngine;
using System.Collections;

public class CGameManager : MonoBehaviour
{
	//public GameObject[] arrGameObjectSpawnPoints;
	public CSharedData  m_sharedData;
	public StateMachine m_stateMachine;

	void Awake()
	{
		AwakeInitializeSharedData();
		AwakeSetUpStates();

	}

	private void AwakeInitializeSharedData()
	{
		this.m_sharedData = new CSharedData();
	}

	private void AwakeSetUpStates()
	{
		m_stateMachine =
			gameObject.AddComponent<StateMachine>(); 

		State stateInit   = m_stateMachine.StateInsert( "Init"   );
		State stateUpdate = m_stateMachine.StateInsert( "Update" );

		CTestLevelInitStateBehaviour testLevelInitStateBehaviour =
			gameObject.AddComponent("CTestLevelInitStateBehaviour") as CTestLevelInitStateBehaviour;
		( (StateBehaviour)testLevelInitStateBehaviour ).stateMachineParent = m_stateMachine;
		testLevelInitStateBehaviour.m_sharedData = this.m_sharedData;
		
		CTestLevelUpdateStateBehaviour testLevelUpdateStateBehaviour =
			gameObject.AddComponent("CTestLevelUpdateStateBehaviour") as CTestLevelUpdateStateBehaviour;
		( (StateBehaviour)testLevelUpdateStateBehaviour ).stateMachineParent = m_stateMachine;
		testLevelUpdateStateBehaviour.m_sharedData = this.m_sharedData;

		stateInit.ScriptInsert( testLevelInitStateBehaviour );		
		stateUpdate.ScriptInsert( testLevelUpdateStateBehaviour );

		m_stateMachine.stateActive = "Init";
	}


}