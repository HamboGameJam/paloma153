using UnityEngine;
using System.Collections;

public class CPlayablePaloma : MonoBehaviour
{
	public string id;
	public ExtendedController m_extendedController;
	public CPaloma            m_paloma;
	public StateMachine       m_stateMachine;

	void Awake()
	{
		m_extendedController =
			gameObject.GetComponent<ExtendedController>();

		m_extendedController.gravity      =  9.80f;
		m_extendedController.jumpHeight   =  5.00f;
		m_extendedController.jumpSpeed    = 05.00f;
		m_extendedController.runSpeed     = 06.00f;
		m_extendedController.walkSpeed    = 02.00f;

		m_paloma = 
			gameObject.GetComponent<CPaloma>();
		
		///////////////////////////////////////
		// Create State Machine 
		m_stateMachine =
			gameObject.AddComponent<StateMachine>();

		///////////////////////////////////////
		// Create the States
		// - A state is created by simpply Naming it... 
		State stateBeaking     = m_stateMachine.StateInsert( "Beaking"  );
		State stateBreaking    = m_stateMachine.StateInsert( "Breaking" );
		State stateFalling     = m_stateMachine.StateInsert( "Falling"  );
		State stateFlapping    = m_stateMachine.StateInsert( "Flapping" );
		State stateIdle        = m_stateMachine.StateInsert( "Idle"     );
		State stateJumping     = m_stateMachine.StateInsert( "Jumping"  );
		State stateLanding     = m_stateMachine.StateInsert( "Landing"  );
		State stateRunning     = m_stateMachine.StateInsert( "Running"  );
		State stateWalking     = m_stateMachine.StateInsert( "Walking"  );

		///////////////////////////////////////
		// Load the Scripts
		// - Add Component to create an instantiation of the script
		// - Assign the reference of the instantiated StateMachine to the StateMachineParent
		//   In order to indicate to the state machine to change the state within another state script

		CPalomaIdleStateBehaviour palomaIdleStateBehaviour =
			gameObject.AddComponent("CPalomaIdleStateBehaviour") as CPalomaIdleStateBehaviour;
		( (StateBehaviour)palomaIdleStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaIdleStateBehaviour.m_playablePaloma = this;

		CPlayablePalomaIdleStateBehaviour playablePalomaIdleStateBehaviour =
			gameObject.AddComponent("CPlayablePalomaIdleStateBehaviour") as CPlayablePalomaIdleStateBehaviour;
		( (StateBehaviour)playablePalomaIdleStateBehaviour ).stateMachineParent = m_stateMachine;
		playablePalomaIdleStateBehaviour.m_playablePaloma = this;
		
		CPalomaBeakingStateBehaviour palomaBeakingStateBehaviour =
			gameObject.AddComponent("CPalomaBeakingStateBehaviour") as CPalomaBeakingStateBehaviour;
		( (StateBehaviour)palomaBeakingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaBeakingStateBehaviour.m_playablePaloma = this;

		CPalomaBreakingStateBehaviour palomaBreakingStateBehaviour =
			gameObject.AddComponent("CPalomaBreakingStateBehaviour") as CPalomaBreakingStateBehaviour;
		( (StateBehaviour)palomaBreakingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaBreakingStateBehaviour.m_playablePaloma = this;
		
		CPalomaFallingStateBehaviour palomaFallingStateBehaviour =
			gameObject.AddComponent("CPalomaFallingStateBehaviour") as CPalomaFallingStateBehaviour;
		( (StateBehaviour)palomaFallingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaFallingStateBehaviour.m_playablePaloma = this;

		CPalomaJumpingStateBehaviour palomaJumpingStateBehaviour =
			gameObject.AddComponent("CPalomaJumpingStateBehaviour") as CPalomaJumpingStateBehaviour;
		( (StateBehaviour)palomaJumpingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaJumpingStateBehaviour.m_playablePaloma = this;

		CPlayablePalomaFallingStateBehaviour playablePalomaFallingStateBehaviour =
			gameObject.AddComponent("CPlayablePalomaFallingStateBehaviour") as CPlayablePalomaFallingStateBehaviour;
		( (StateBehaviour)playablePalomaFallingStateBehaviour ).stateMachineParent = m_stateMachine;
		playablePalomaFallingStateBehaviour.m_playablePaloma = this;

		CPlayablePalomaFlappingStateBehaviour playablePalomaFlappingStateBehaviour =
			gameObject.AddComponent("CPlayablePalomaFlappingStateBehaviour") as CPlayablePalomaFlappingStateBehaviour;
		( (StateBehaviour)playablePalomaFlappingStateBehaviour ).stateMachineParent = m_stateMachine;
		playablePalomaFlappingStateBehaviour.m_playablePaloma = this;
		
		CPalomaFlappingStateBehaviour palomaFlappingStateBehaviour =
			gameObject.AddComponent("CPalomaFlappingStateBehaviour") as CPalomaFlappingStateBehaviour;
		( (StateBehaviour)palomaFlappingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaFlappingStateBehaviour.m_playablePaloma = this;

		CPlayablePalomaRunningStateBehaviour playablePalomaRunningStateBehaviour =
			gameObject.AddComponent("CPlayablePalomaRunningStateBehaviour") as CPlayablePalomaRunningStateBehaviour;
		( (StateBehaviour)playablePalomaRunningStateBehaviour ).stateMachineParent = m_stateMachine;
		playablePalomaRunningStateBehaviour.m_playablePaloma = this;
		
		CPalomaRunningStateBehaviour palomaRunningStateBehaviour =
			gameObject.AddComponent("CPalomaRunningStateBehaviour") as CPalomaRunningStateBehaviour;
		( (StateBehaviour)palomaRunningStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaRunningStateBehaviour.m_playablePaloma = this;

		CPlayablePalomaWalkingStateBehaviour playablePalomaWalkingStateBehaviour =
			gameObject.AddComponent("CPlayablePalomaWalkingStateBehaviour") as CPlayablePalomaWalkingStateBehaviour;
		( (StateBehaviour)playablePalomaWalkingStateBehaviour ).stateMachineParent = m_stateMachine;
		playablePalomaWalkingStateBehaviour.m_playablePaloma = this;

		CPalomaWalkingStateBehaviour palomaWalkingStateBehaviour =
			gameObject.AddComponent("CPalomaWalkingStateBehaviour") as CPalomaWalkingStateBehaviour;
		( (StateBehaviour)palomaWalkingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaWalkingStateBehaviour.m_playablePaloma = this;

		CPalomaLandingStateBehaviour palomaLandingStateBehaviour =
			gameObject.AddComponent("CPalomaLandingStateBehaviour") as CPalomaLandingStateBehaviour;
		( (StateBehaviour)palomaLandingStateBehaviour ).stateMachineParent = m_stateMachine;
		palomaLandingStateBehaviour.m_playablePaloma = this;

		///////////////////////////////////////
		// Assignt the scripts to the states
		// - There can be only one state active at a time within a particular state machine
		// - A single instantiation of a script can be assigned to multiple states
		stateBeaking.ScriptInsert( palomaBeakingStateBehaviour );

		stateBreaking.ScriptInsert( palomaBreakingStateBehaviour );

		stateFalling.ScriptInsert( playablePalomaFallingStateBehaviour );
		stateFalling.ScriptInsert( palomaFallingStateBehaviour );
		
		stateFlapping.ScriptInsert( palomaFlappingStateBehaviour );
		stateFlapping.ScriptInsert( playablePalomaFlappingStateBehaviour );

		stateIdle.ScriptInsert( palomaIdleStateBehaviour );
		stateIdle.ScriptInsert( playablePalomaIdleStateBehaviour );

		stateJumping.ScriptInsert( palomaJumpingStateBehaviour );

		stateLanding.ScriptInsert( palomaLandingStateBehaviour );

		stateRunning.ScriptInsert( palomaRunningStateBehaviour );
		stateRunning.ScriptInsert( playablePalomaRunningStateBehaviour );

		stateWalking.ScriptInsert( palomaWalkingStateBehaviour );
		stateWalking.ScriptInsert( playablePalomaWalkingStateBehaviour );
		
		//////////////////////////////////////
		// set the previous state and the current one
		m_stateMachine.stateActive = "Falling";
	}


}