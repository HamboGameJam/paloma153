using UnityEngine;
using System.Collections;

public class CPalomaWalkingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		m_playablePaloma.m_extendedController.currentSpeed =
			m_playablePaloma.m_extendedController.walkSpeed;

 

		m_playablePaloma.m_paloma.bIsWalking =
			true;

		Debug.Log("CPalomaWalkingStateBehaviour:OnEnabled");

	}
	
	override public void OnDisabled()
	{
		m_playablePaloma.m_paloma.bIsWalking = false;

		this.enabled = false;
		Debug.Log("CPalomaWalkingStateBehaviour:OnDisabled");
	}
	
	void Start()
	{
		
	}
	
	
	void FixedUpdate()
	{

		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		      CollisionFlags.CollidedBelow                                ) == 0  )
		{
			m_statemachineParentReference.stateActive = "Falling";
		}
		else
		{
			m_playablePaloma.m_extendedController.verticalSpeed = 0.00f;
		}
		
	}
	
}