using UnityEngine;
using System.Collections;

public class CPalomaFlappingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;

		this.m_playablePaloma.m_paloma.bIsOnAir = true;

		m_playablePaloma.m_extendedController.verticalSpeed =
			m_playablePaloma.m_extendedController.jumpHeight;

		m_playablePaloma.m_extendedController.m_fSpeedCurrent *= 1.25f;
		//m_playablePaloma.m_paloma.bIsFlapping = true;

		Debug.Log("CPalomaFlappingStateBehaviour:OnEnabled");
	}
	
	override public void OnDisabled()
	{
		m_playablePaloma.m_paloma.bIsFlapping = false;
		//m_playablePaloma.m_extendedController.m_fSpeedCurrent *= .75f;
		this.enabled = false;

		Debug.Log("CPalomaFlappingStateBehaviour:OnDisabled");
	}
	
	void Start()
	{
		
	}
	
	
	void Update()
	{
		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0  )
		{
			Debug.Log("Still Falling");
		}
		else
		{
			Debug.Log("Landing");
			this.m_playablePaloma.m_paloma.bIsOnAir = false;
			m_statemachineParentReference.stateActive = "Landing";
			
		}
	}
	
}