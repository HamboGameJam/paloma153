﻿using UnityEngine;
using System.Collections;

public class CPalomaFallingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		m_playablePaloma.m_paloma.bIsOnAir = true;
		m_playablePaloma.m_paloma.bIsFlapping = false;
		m_playablePaloma.m_paloma.bIsFalling = true;
		m_playablePaloma.m_extendedController.isMovingInGround = false;

		Debug.Log("CPalomaFallingStateBehaviour:OnEnabled");
		
	}

	override public void OnDisabled()
	{
		//m_playablePaloma.m_paloma.bIsFalling = false;
		//m_playablePaloma.m_paloma.bIsOnAir = false;
		this.enabled = false;
		Debug.Log("CPalomaFallingStateBehaviour:OnDisabled");
	}
	
	void Start()
	{
		
	}
	
	
	void Update()
	{
		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0 )
		{
			Debug.Log("Still Falling");
		}
		else
		{
			Debug.Log("Landing");

			m_statemachineParentReference.stateActive = "Landing";

		}
	/*
		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );

		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0 )
		{
			if(  animatorStateInfoCurrent.nameHash !=
			     Animator.StringToHash("Base Layer.stateBreaking")  )
			{
				m_statemachineParentReference.stateActive = "Falling";
			}	
		}
		else
		{
			if(  animatorStateInfoCurrent.nameHash !=
			     Animator.StringToHash("Base Layer.stateIdle")  )
			{
				m_statemachineParentReference.stateActive = "Idle";
			}		
		}
	*/		
	}
	
}
