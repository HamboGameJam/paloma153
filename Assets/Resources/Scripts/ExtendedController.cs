using UnityEngine;
using System.Collections;

[AddComponentMenu("Physics/Extended Controller")]

public class ExtendedController : MonoBehaviour
{
	void Start()
	{
		m_bCanJump                       = true;
		m_bIsMovingInGround              = false;
		m_fSpeedCurrent                  = 00.00f;
		m_fSpeedVertical                 = 00.00f;
		m_v3CurrentJumpStartingPosition  = Vector3.zero;
		m_v3CurrentDirection             = Vector3.zero;

		m_characterController            = gameObject.GetComponent<CharacterController>();
		m_collisionFlagsLast             = CollisionFlags.None;
	}

	void Update()
	{
		Vector3 v3ThisFrameMovement;
		Vector3 v3ThisVerticalMovement;

		if( false == isMovingInGround )
		{
			m_fSpeedVertical = m_fSpeedVertical - ( gravity * Time.deltaTime );
		}
		else
		{
			m_fSpeedVertical = 0.00f;
		}

		m_v3CurrentDirection = gameObject.transform.right;
		m_v3CurrentDirection = m_v3CurrentDirection.normalized;
		
		v3ThisVerticalMovement.x = 0.00f;
		v3ThisVerticalMovement.y = m_fSpeedVertical;
		v3ThisVerticalMovement.z = 0.00f;
		
		v3ThisFrameMovement = (  ( m_v3CurrentDirection * -m_fSpeedCurrent ) +
								 ( v3ThisVerticalMovement                  )    );
		v3ThisFrameMovement *= Time.deltaTime;
		
		m_collisionFlagsLast = m_characterController.Move(v3ThisFrameMovement);

	}
	
	public bool canJump
	{
		get { return m_bCanJump;  }
		set { m_bCanJump = value; }
	}
	
	public Vector3 currentDirection
	{
		get { return m_v3CurrentDirection;  }
		set { m_v3CurrentDirection = value; }
	}
	
	public Vector3 currentJumpStartingPosition
	{
		get { return m_v3CurrentJumpStartingPosition;  }
		set { m_v3CurrentJumpStartingPosition = value; }
	}
	
	public float currentSpeed
	{
		get { return m_fSpeedCurrent; }
		set { m_fSpeedCurrent = value; }
	}

	public float groundSpeed
	{
		get { return m_fSpeedGround; }
		set { m_fSpeedGround = value; }
	}
	
	public bool isGrounded
	{
		get { return ( m_collisionFlagsLast & CollisionFlags.CollidedBelow ) != 0; }
	}

	public bool isMovingInGround
	{
		get { return m_bIsMovingInGround; }
		set { m_bIsMovingInGround = value; }
	}
	
	public CollisionFlags lastCollisionFlags
	{
		get { return m_collisionFlagsLast;  }
		set { m_collisionFlagsLast = value; }
	}
	
	public float verticalSpeed
	{
		get{ return m_fSpeedVertical;  }
		set{ m_fSpeedVertical = value; }
	}

	private bool                m_bCanJump;
	public bool                m_bIsMovingInGround;
	public  CollisionFlags      m_collisionFlagsLast;
	private CharacterController m_characterController;
	public  float               m_fSpeedCurrent;
	public float                m_fSpeedVertical;

	private float               m_fSpeedGround;
	private Vector3             m_v3CurrentDirection;
	private Vector3             m_v3CurrentJumpStartingPosition;

	public float                gravity;
	public float                jumpHeight;
	public float                jumpSpeed;
	public float                runSpeed;
	public float                walkSpeed;
	
}