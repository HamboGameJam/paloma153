﻿using UnityEngine;
using System.Collections;

public class CCameraFollow : MonoBehaviour
{
	public  GameObject m_gameObjectTarget;
	private Camera     m_camera;
	private Rect       m_rectPlaceholder;
	//private Vector3  m_v3PositionCurrent;
	public Vector3    m_v3PositionDestination;
	//private 
	void Awake()
	{
		m_camera = gameObject.GetComponent<Camera>();
		m_v3PositionDestination = new Vector3( 0.00f, 0.00f, -40.00f );
	}

	private void LateUpdate_MoveCamera()
	{
		m_v3PositionDestination.x = m_gameObjectTarget.transform.position.x;
		m_v3PositionDestination.y = m_gameObjectTarget.transform.position.y;

		transform.position =
			Vector3.MoveTowards(  transform.position,
			                      m_v3PositionDestination,
			                     ( Time.deltaTime * 10.0f ) );
	}

	void LateUpdate()
	{
		LateUpdate_MoveCamera();
	}

	public float fViewPortRectX
	{
		get
		{
			return m_camera.rect.x;
		}

		set
		{
			m_rectPlaceholder = m_camera.rect;
			m_rectPlaceholder.x = value;
			m_camera.rect = m_rectPlaceholder;
		}

	}

	public float fViewPortRectY
	{
		get
		{
			return m_camera.rect.y;
		}
		
		set
		{
			m_rectPlaceholder = m_camera.rect;
			m_rectPlaceholder.y = value;
			m_camera.rect = m_rectPlaceholder;
		}
		
	}

	public float fViewPortRectWidth
	{
		get
		{
			return m_camera.rect.width;
		}
		
		set
		{
			m_rectPlaceholder = m_camera.rect;
			m_rectPlaceholder.width = value;
			m_camera.rect = m_rectPlaceholder;
		}
		
	}
	
	public float fViewPortRectHeight
	{
		get
		{
			return m_camera.rect.height;
		}
		
		set
		{
			m_rectPlaceholder = m_camera.rect;
			m_rectPlaceholder.height = value;
			m_camera.rect = m_rectPlaceholder;
		}
		
	}

}
