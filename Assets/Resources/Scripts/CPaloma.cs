﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CPaloma : MonoBehaviour {

	public Animator animator;

	//bool bIsWalking;
	//bool bIsRunning;
	//bool bIsJumping;
	//bool bIsOnAir;
	//bool bIsFlapping;
	//bool bIsFalling;
	//bool bIsBreaking;

	public bool bIsFacingLeft
	{
		get
		{
			return( gameObject.transform.localRotation.y == 0.00f );
		}

		set
		{
			if( value == true )
			{
				gameObject.transform.localRotation =
					Quaternion.Euler( 0.00f, 0.00f, 0.00f ); //Vector3( 0.00f, 0.00f, 0.00f )  );
			}
			else
			{
				gameObject.transform.localRotation =
					Quaternion.Euler( 0.00f, 180.00f, 0.00f ); //Vector3( 0.00f, 180.00f, 0.00f )  );
			}
		}
	}

	public bool bIsBeaking
	{
		get
		{
			return animator.GetBool( "bIsBeaking" );
		}
		
		set
		{
			animator.SetBool( "bIsBeaking", value );
		}
	}

	public bool bIsBreaking
	{
		get
		{
			return animator.GetBool( "bIsBreaking" );
		}
		
		set
		{
			animator.SetBool( "bIsBreaking", value );
		}
	}

	public bool bIsFalling
	{
		get
		{
			return animator.GetBool( "bIsFalling" );
		}
		
		set
		{
			animator.SetBool( "bIsFalling", value );
		}
	}

	public bool bIsFlapping
	{
		get
		{
			return animator.GetBool( "bIsFlapping" );
		}
		
		set
		{
			animator.SetBool( "bIsFlapping", value );
		}
	}

	public bool bIsLanding
	{
		get
		{
			return animator.GetBool( "bIsLanding" );
		}
		
		set
		{
			animator.SetBool( "bIsLanding", value );
		}
	}

	public bool bIsWalking
	{
		get
		{
			return animator.GetBool( "bIsWalking" );
		}

		set
		{
			animator.SetBool( "bIsWalking", value );
		}
	}

	public bool bIsRunning
	{
		get
		{
			return animator.GetBool( "bIsRunning" );
		}
		
		set
		{
			animator.SetBool( "bIsRunning", value );
		}
	}
	
	public bool bIsJumping
	{
		get
		{
			return animator.GetBool( "bIsJumping" );
		}
		
		set
		{
			animator.SetBool( "bIsJumping", value );
		}
	}

	public bool bIsOnAir
	{
		get
		{
			return animator.GetBool( "bIsOnAir" );
		}
		
		set
		{
			animator.SetBool( "bIsOnAir", value );
		}
	}

	public float fVerticalSpeed
	{
		get
		{
			return animator.GetFloat( "fVerticalSpeed" );
		}
		
		set
		{
			animator.SetFloat( "fVerticalSpeed", value );
		}
	}

	void Awake()
	{
		animator = this.GetComponent<Animator>(  );
		//animator = Instantiate(  Resources.Load( "Animations/animationControllerPaloma", typeof(Animator) )  ) as Animator;
	}

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
