﻿using UnityEngine;
using System.Collections;

public class CEgg : MonoBehaviour
{

	public Animator animator;

	void Awake()
	{
		animator = this.GetComponent<Animator>(  );
	}

	void Update()
	{
	
	}

	public bool bIsSpawning
	{
		get
		{
			return animator.GetBool( "bIsSpawning" );
		}
		
		set
		{
			animator.SetBool( "bIsSpawning", value );
		}
	}

	public bool bIsBreaking
	{
		get
		{
			return animator.GetBool( "bIsBreaking" );
		}
		
		set
		{
			animator.SetBool( "bIsBreaking", value );
		}
	}
	
	public bool bIsOnAir
	{
		get
		{
			return animator.GetBool( "bIsOnAir" );
		}
		
		set
		{
			animator.SetBool( "bIsOnAir", value );
		}
	}

	public float fVerticalSpeed
	{
		get
		{
			return animator.GetFloat( "fVerticalSpeed" );
		}
		
		set
		{
			animator.SetFloat( "fVerticalSpeed", value );
		}
	}

}
