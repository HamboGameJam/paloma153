﻿using UnityEngine;
using System.Collections;

public class CPalomaBreakingStateBehaviour : StateBehaviour
{
	public CPlayablePaloma m_playablePaloma;
	
	override public void OnEnabled()
	{
		this.enabled = true;

		m_playablePaloma.m_extendedController.currentSpeed *= 0.25f;

		m_playablePaloma.m_paloma.bIsRunning = false;

		m_playablePaloma.m_paloma.bIsBreaking = true;



		Debug.Log("CPalomaBreakingStateBehaviour:OnEnabled");

	}
	
	override public void OnDisabled()
	{
		this.enabled = false;
		Debug.Log("CPalomaBreakingStateBehaviour:OnDisabled");
	}

	void Start()
	{
	
	}
	
	void Update()
	{
		m_playablePaloma.m_extendedController.currentSpeed *= 0.20f;

		AnimatorStateInfo animatorStateInfoCurrent =
			m_playablePaloma.m_paloma.animator.GetCurrentAnimatorStateInfo( 0 );

		if(  ( m_playablePaloma.m_extendedController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0 )
		{
			if(  animatorStateInfoCurrent.nameHash !=
			     Animator.StringToHash("Base Layer.stateBreaking")  )
			{
				m_statemachineParentReference.stateActive = "Falling";
			}	
		}
		else
		{
			if(  m_playablePaloma.m_extendedController.currentSpeed < 0.0001f  )
			{
				m_statemachineParentReference.stateActive = "Idle";
			}		
		}

	}


}
