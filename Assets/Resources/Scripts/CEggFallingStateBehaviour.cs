﻿using UnityEngine;
using System.Collections;

public class CEggFallingStateBehaviour : StateBehaviour
{
	public CPlayableEgg m_playableEgg;
	
	override public void OnEnabled()
	{
		this.enabled = true;
		this.m_playableEgg.m_egg.bIsOnAir    = true;
		this.m_playableEgg.m_egg.bIsBreaking = false;
		this.m_playableEgg.m_egg.bIsSpawning = false;
				
		Debug.Log("CEggFallingStateBehaviour:OnEnabled");
		
	}
	
	
	override public void OnDisabled()
	{
		this.enabled = false;
	}


	void Start() 
	{
		
	}
	

	private void FixedUpdate_ApplyGravityCollision()
	{
		if(  Mathf.Abs( this.m_playableEgg.m_eggController.verticalSpeed ) > 1.00  )
		{
			this.m_playableEgg.m_eggController.m_fHealth -=
				Mathf.Abs( this.m_playableEgg.m_eggController.verticalSpeed * 0.001f );
		}

		if( this.m_playableEgg.m_eggController.m_fHealth > 0.00f)
		{
			m_statemachineParentReference.stateActive = "Grounded";
		}
		else
		{
			m_statemachineParentReference.stateActive = "Breaking";
		}

	}


	void FixedUpdate()
	{
		if(  ( this.m_playableEgg.m_eggController.m_collisionFlagsLast&
		       CollisionFlags.CollidedBelow                                ) == 0 )
		{
			Debug.Log("Still Falling");
		}
		else
		{
			Debug.Log("Landing");

			FixedUpdate_ApplyGravityCollision();
			
		}
	}


	void OnTriggerEnter( Collider colliderOther )
	{
		if( colliderOther.gameObject.tag == "Spikes" )
		{
			m_statemachineParentReference.stateActive = "Breaking";
		}
	}	

}